package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.frontend;
import views.html.index;

public class Frontend extends Controller {

    public static Result index() {
        return ok(frontend.render());
    }

}
