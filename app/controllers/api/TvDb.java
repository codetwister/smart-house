package controllers.api;

import play.mvc.Result;
import ro.codetwisters.home.application.TvdbOperations;

/**
 */
public class TvDb extends ApiController {

    public static Result searchForMovie(String searchString) {
        ApiResponse resp = new ApiResponse(TvdbOperations.getResultsFromTvdbApi(searchString));
        return ok(gson.toJson(resp)).as("application/json");
    }

    public static Result loadMovie(long tvdbId) {
        boolean success = TvdbOperations.loadTvdbMovie(tvdbId);
        if (success) {
            return createResponse("Success");
        } else {
            return createError(-1, "Movie could not be loaded from TvDb");
        }
    }

}
