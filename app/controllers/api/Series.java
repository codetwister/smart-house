package controllers.api;

import models.Episode;
import play.mvc.Result;

/**
 */
public class Series extends ApiController {

    public static Result getSeriesList() {
        return createResponse(models.Series.find.all());
    }

    public static Result getEpisodes(Long seriesId) {
        return createResponse(Episode.find.where().eq("seriesId", seriesId).findList());
    }

}
