package controllers.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import play.mvc.Controller;
import play.mvc.Result;
import ro.codetwisters.home.application.GsonExlusionStrategy;

/**
 */
public class ApiController extends Controller {

    protected static final Gson gson = new GsonBuilder()
            .setExclusionStrategies(new GsonExlusionStrategy())
            .serializeNulls()
            .create();

    protected static final Result createResponse(Object response) {
        return ok(gson.toJson(new ApiResponse(response))).as("application/json");
    }

    protected static final Result createError(int code, String message) {
        return ok(gson.toJson(new ApiResponse(new ApiResponse.ApiError(code, message)))).as("application/json");
    }

}
