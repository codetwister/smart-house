package controllers.api;

import ro.codetwisters.home.application.JsonField;

/**
 * this needs to be the format of every api response.
 */
public class ApiResponse {
    @JsonField
    private Object result;

    @JsonField
    private ApiError error;

    public ApiResponse(Object result) {
        this.result = result;
    }

    public ApiResponse(ApiError error) {
        this.error = error;
    }

    public static class ApiError {
        private int code;
        private String message;

        public ApiError(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }
}
