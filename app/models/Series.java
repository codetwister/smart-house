package models;

import play.db.ebean.Model;
import ro.codetwisters.home.application.JsonField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 */
@Entity
public class Series extends Model {

    @Id
    @JsonField
    private Long id;
    @JsonField
    private float rating;
    @JsonField
    private String title;
    @JsonField
    private String genre;
    @JsonField
    private String actors;
    @JsonField
    private String imdbId;
    @JsonField
    private long tvdbId;
    @Column(columnDefinition = "TEXT")
    @JsonField
    private String description;
    @JsonField
    private long lastUpdateTdvb; // last update from tvdb api
    @JsonField
    private String posterPath;

    public static Finder<Long, Series> find = new Finder<Long, Series>(
            Long.class, Series.class
    );

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public long getTvdbId() {
        return tvdbId;
    }

    public void setTvdbId(long tvdbId) {
        this.tvdbId = tvdbId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getLastUpdateTdvb() {
        return lastUpdateTdvb;
    }

    public void setLastUpdateTdvb(long lastUpdateTdvb) {
        this.lastUpdateTdvb = lastUpdateTdvb;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }
}
