package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User extends Model {

    @Id
    private Long id;
    private String email;
    private String password;
    private String watchedEpisodes;

    public static Finder<Long, User> find = new Finder<Long, User>(
            Long.class, User.class
    );
}
