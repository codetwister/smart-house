package models;

import play.db.ebean.Model;
import ro.codetwisters.home.application.JsonField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 */
@Entity
public class Episode extends Model {

    @Id
    @JsonField
    private Long id;
    @JsonField
    private long seriesId;
    @JsonField
    private float rating;
    @JsonField
    private String title;
    @JsonField
    private String aired;
    @JsonField
    private long tvdbId;
    @Column(columnDefinition = "TEXT")
    @JsonField
    private String description;
    @JsonField
    private int episodeNumber;
    @JsonField
    private int seasonNumber;
    @JsonField
    private String thumbPath;

    public static Finder<Long, Episode> find = new Finder<Long, Episode>(
            Long.class, Episode.class
    );

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(long seriesId) {
        this.seriesId = seriesId;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAired() {
        return aired;
    }

    public void setAired(String aired) {
        this.aired = aired;
    }

    public long getTvdbId() {
        return tvdbId;
    }

    public void setTvdbId(long tvdbId) {
        this.tvdbId = tvdbId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }
}
