package ro.codetwisters.home.application;

import models.Episode;
import models.Series;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 */
public class TvdbOperations {

    private static final String TVDB_API_KEY = "CF156900FDE42EF1";

    public static boolean loadTvdbMovie(long tvdbId) {
        TvdbResult result = getSeriesInfo(tvdbId);
        // insert data into db
        result.getSeries().setLastUpdateTdvb(System.currentTimeMillis());
        result.getSeries().save();
        long seriesInternalId = result.getSeries().getId();
        for (Episode episode : result.getEpisodes()) {
            episode.setSeriesId(seriesInternalId);

            Episode savedEpisode = Episode.find.where().eq("tvdbId", episode.getTvdbId()).findUnique();
            if (savedEpisode != null) {
                episode.setId(savedEpisode.getId());
            }
            episode.save();
        }
        return seriesInternalId > 0;
    }

    private static TvdbResult getSeriesInfo(long seriesId) {
        try {
            URL tvdbUrl = new URL(String.format("http://thetvdb.com/api/%s/series/%d/all/en.xml",
                    TVDB_API_KEY, seriesId));
            HttpURLConnection urlConnection = (HttpURLConnection) tvdbUrl.openConnection();

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            SeriesItemHandler myXMLHandler = new SeriesItemHandler();
            xr.setContentHandler(myXMLHandler);
            InputSource inStream = new InputSource();
            inStream.setByteStream(urlConnection.getInputStream());
            xr.parse(inStream);
            return new TvdbResult(myXMLHandler.getSeries(), myXMLHandler.getEpisodes());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<TvdbSearchResult> getResultsFromTvdbApi(String searchString) {
        try {
            URL tvdbUrl = new URL(String.format("http://thetvdb.com/api/GetSeries.php?seriesname=%s",
                    URLEncoder.encode(searchString, "UTF-8")));
            HttpURLConnection urlConnection = (HttpURLConnection) tvdbUrl.openConnection();

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            SearchResultItemHandler myXMLHandler = new SearchResultItemHandler();
            xr.setContentHandler(myXMLHandler);
            InputSource inStream = new InputSource();
            inStream.setByteStream(urlConnection.getInputStream());
            xr.parse(inStream);
            return myXMLHandler.getResults();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class TvdbSearchResult {

        @JsonField
        long seriesId;
        @JsonField
        String title;
        @JsonField
        String description;

        public long getSeriesId() {
            return seriesId;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }
    }

    public static class TvdbResult {
        private Series series;
        private List<Episode> episodes;

        public TvdbResult(Series series, List<Episode> episodes) {
            this.series = series;
            this.episodes = episodes;
        }

        public Series getSeries() {
            return series;
        }

        public List<Episode> getEpisodes() {
            return episodes;
        }
    }

    private static class SeriesItemHandler extends DefaultHandler {

        private String characters;
        private Series series = new Series();
        private List<Episode> episodes = new ArrayList<Episode>();
        private Episode currentEpisode;
        private boolean parsingEpisode;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);
            characters = "";
            if (qName.equals("Series")) {
                parsingEpisode = false;
            } else if (qName.equals("Episode")) {
                parsingEpisode = true;
                currentEpisode = new Episode();
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);
            if (qName.equals("Episode")) {
                episodes.add(currentEpisode);
            } else if (parsingEpisode) {
                if (qName.equals("id")) {
                    try {
                        currentEpisode.setTvdbId(Long.parseLong(characters));
                    } catch (NumberFormatException e) {
                        // no-op
                    }
                } else if (qName.equals("Rating")) {
                    try {
                        currentEpisode.setRating(Float.parseFloat(characters));
                    } catch (NumberFormatException e) {
                        // no-op
                    }
                } else if (qName.equals("EpisodeName")) {
                    currentEpisode.setTitle(characters);
                } else if (qName.equals("FirstAired")) {
                    currentEpisode.setAired(characters);
                } else if (qName.equals("Overview")) {
                    currentEpisode.setDescription(characters);
                } else if (qName.equals("EpisodeNumber")) {
                    try {
                        currentEpisode.setEpisodeNumber(Integer.parseInt(characters));
                    } catch (NumberFormatException e) {
                        // no-op
                    }
                } else if (qName.equals("SeasonNumber")) {
                    try {
                        currentEpisode.setSeasonNumber(Integer.parseInt(characters));
                    } catch (NumberFormatException e) {
                        // no-op
                    }
                } else if (qName.equals("filename")) {
                    currentEpisode.setThumbPath(characters);
                }
            } else {
                // parsing series
                if (qName.equals("id")) {
                    try {
                        series.setTvdbId(Long.parseLong(characters));
                    } catch (NumberFormatException e) {
                        // no-op
                    }
                } else if (qName.equals("Overview")) {
                    series.setDescription(characters);
                } else if (qName.equals("IMDB_ID")) {
                    series.setImdbId(characters);
                } else if (qName.equals("Actors")) {
                    series.setActors(characters);
                } else if (qName.equals("Genre")) {
                    series.setGenre(characters);
                } else if (qName.equals("SeriesName")) {
                    series.setTitle(characters);
                } else if (qName.equals("Rating")) {
                    try {
                        series.setRating(Float.parseFloat(characters));
                    } catch (NumberFormatException e) {
                        // no-op
                    }
                } else if (qName.equals("poster")) {
                    series.setPosterPath(characters);
                }
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            super.characters(ch, start, length);
            characters += new String(ch, start, length);
        }

        public Series getSeries() {
            return series;
        }

        public List<Episode> getEpisodes() {
            return episodes;
        }
    }

    private static class SearchResultItemHandler extends DefaultHandler {

        private ArrayList<TvdbSearchResult> results = new ArrayList<TvdbSearchResult>();
        private TvdbSearchResult currentResult;
        private String characters;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);
            characters = "";
            if (qName.equals("Series")) {
                currentResult = new TvdbSearchResult();
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);
            if (qName.equals("Series")) {
                results.add(currentResult);
            } else if (qName.equals("seriesid")) {
                currentResult.seriesId = Long.parseLong(characters);
            } else if (qName.equals("SeriesName")) {
                currentResult.title = characters;
            } else if (qName.equals("Overview")) {
                currentResult.description = characters;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            super.characters(ch, start, length);
            characters += new String(ch, start, length);
        }

        public ArrayList<TvdbSearchResult> getResults() {
            return results;
        }
    }
}
