package ro.codetwisters.home.modules;

import java.util.ArrayList;

/**
 */
public class ConsoleOutputModule extends AbstractModule {

    @Override
    protected void configureIO() {
        inputs = new ArrayList<ModuleInput>();
        inputs.add(new ModuleInput("consoleIn", this));
    }

    @Override
    public void inputValueChanged(ModuleInput input, Object oldValue, Object newValue) {
        super.inputValueChanged(input, oldValue, newValue);
        System.out.println(newValue);
    }
}
