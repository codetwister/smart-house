package ro.codetwisters.home.modules;

import java.util.ArrayList;

/**
 */
public class ConstantValueOutputModule<T> extends AbstractModule {

    private T data;

    public ConstantValueOutputModule(T data) {
        this.data = data;
        getOutputById("constant").writeValue(data);
    }

    @Override
    protected void configureIO() {
        outputs = new ArrayList<ModuleOutput>();
        outputs.add(new ModuleOutput("constant"));
    }

    public static ConstantValueOutputModule ofInt(int i) {
        return new ConstantValueOutputModule<Integer>(i);
    }
}
