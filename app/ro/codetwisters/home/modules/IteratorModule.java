package ro.codetwisters.home.modules;

import java.util.ArrayList;

/**
 */
public class IteratorModule extends AbstractModule {

    @Override
    protected void configureIO() {
        inputs = new ArrayList<ModuleInput>();
        inputs.add(new ModuleInput("start", this));
        inputs.add(new ModuleInput("end", this));
        outputs = new ArrayList<ModuleOutput>();
        outputs.add(new ModuleOutput("current"));
    }

    public void doIteration() {
        int start = getInputById("start").readValue(Integer.class);
        int end = getInputById("end").readValue(Integer.class);
        for (int i = start; i <= end; i++) {
            getOutputById("current").writeValue(i);
        }
    }
}
