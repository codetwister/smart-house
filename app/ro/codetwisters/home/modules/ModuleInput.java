package ro.codetwisters.home.modules;

/**
 */
public class ModuleInput {

    private String inputId;

    private ModuleOutput output;
    private Object data;
    private InputChangedListener listener;

    public ModuleInput(String inputId, InputChangedListener listener) {
        this.inputId = inputId;
        this.listener = listener;
    }

    public void associateOutput(ModuleOutput output) {
        this.output = output;
        if (!output.isAssociatedWith(this)) {
            output.associateInput(this);
        }
    }

    public String getInputId() {
        return inputId;
    }

    public boolean isAssociated() {
        return output != null;
    }

    public void writeValue(Object data) {
        Object oldValue = this.data;
        this.data = data;
        listener.inputValueChanged(this, oldValue, data);
    }

    public <T> T readValue(Class<T> clz) {
        if (data != null && clz.isAssignableFrom(data.getClass())) {
            //noinspection unchecked
            return (T) data;
        }
        return null;
    }

    public interface InputChangedListener {
        public void inputValueChanged(ModuleInput input, Object oldValue, Object newValue);
    }
}
