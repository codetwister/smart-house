package ro.codetwisters.home.modules;

import java.util.List;

/**
 */
public abstract class AbstractModule implements ModuleInput.InputChangedListener {
    protected List<ModuleInput> inputs;
    protected List<ModuleOutput> outputs;

    protected AbstractModule() {
        configureIO();
    }

    protected abstract void configureIO();

    @Override
    public void inputValueChanged(ModuleInput input, Object oldValue, Object newValue) {
        // no need for it to be implemented in modules that don't use it
    }

    public void associateOutput(String outputId, ModuleInput input) {
        getOutputById(outputId).associateInput(input);
    }

    public void associateInput(String inputId, ModuleOutput output) {
        getInputById(inputId).associateOutput(output);
    }

    public ModuleOutput getOutputById(String outputId) {
        for (ModuleOutput output : outputs) {
            if (output.getOutputId().equals(outputId)) {
                return output;
            }
        }
        return null;
    }

    public ModuleInput getInputById(String inputId) {
        for (ModuleInput input : inputs) {
            if (input.getInputId().equals(inputId)){
                return input;
            }
        }
        return null;
    }

    public List<ModuleInput> getInputs() {
        return inputs;
    }

    public List<ModuleOutput> getOutputs() {
        return outputs;
    }
}
