package ro.codetwisters.home.modules;

/**
 */
public class ModuleTest {

    public static void main(String[] args) {
        ConstantValueOutputModule c1 = ConstantValueOutputModule.ofInt(0);
        ConstantValueOutputModule c2 = ConstantValueOutputModule.ofInt(10);
        ConsoleOutputModule console = new ConsoleOutputModule();

        IteratorModule iteratorModule = new IteratorModule();
        iteratorModule.associateInput("start", c1.getOutputById("constant"));
        iteratorModule.associateInput("end", c2.getOutputById("constant"));
        iteratorModule.associateOutput("current", console.getInputById("consoleIn"));

        iteratorModule.doIteration();
    }

}
