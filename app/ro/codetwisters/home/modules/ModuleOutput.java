package ro.codetwisters.home.modules;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class ModuleOutput {

    private String outputId;
    private Object data;
    private List<ModuleInput> associatedInputs;

    public ModuleOutput(String outputId) {
        this.outputId = outputId;
    }

    public void writeValue(Object data) {
        this.data = data;
        if (associatedInputs != null) {
            for (ModuleInput input : associatedInputs) {
                input.writeValue(data);
            }
        }
    }

    public void associateInput(ModuleInput input) {
        if (associatedInputs == null) {
            associatedInputs = new ArrayList<ModuleInput>();
        }
        associatedInputs.add(input);
        if (!input.isAssociated()) {
            input.associateOutput(this);
        }
        input.writeValue(data); // initialize the input with value from output when it's added
    }

    public String getOutputId() {
        return outputId;
    }

    public boolean isAssociatedWith(ModuleInput input) {
        return associatedInputs != null && associatedInputs.contains(input);
    }
}
