# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table episode (
  id                        bigint auto_increment not null,
  series_id                 bigint,
  rating                    float,
  title                     varchar(255),
  aired                     varchar(255),
  tvdb_id                   bigint,
  description               TEXT,
  episode_number            integer,
  season_number             integer,
  thumb_path                varchar(255),
  constraint pk_episode primary key (id))
;

create table series (
  id                        bigint auto_increment not null,
  rating                    float,
  title                     varchar(255),
  genre                     varchar(255),
  actors                    varchar(255),
  imdb_id                   varchar(255),
  tvdb_id                   bigint,
  description               TEXT,
  last_update_tdvb          bigint,
  poster_path               varchar(255),
  constraint pk_series primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  email                     varchar(255),
  password                  varchar(255),
  watched_episodes          varchar(255),
  constraint pk_user primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table episode;

drop table series;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

