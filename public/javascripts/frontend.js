$(function () {

    var Series = Backbone.Model.extend({
        defaults: {
            "title":  "Some title",
            "description":     "some description"
        }
    });

    var SeriesList = Backbone.Collection.extend({
        model: Series
    });

    var SeriesView = Backbone.View.extend({
        tagName:  "li",
        render: function() {
            this.$el.html("T: "+this.model.get("title")+" d: "+this.model.get("description"));
            console.log('html: '+this.$el.html());
            return this;
        }
    });

    var SmartHomeApp = Backbone.View.extend({
        el: $("#SmartHomeApp"),
        initialize: function() {
            this.movies = new SeriesList;

            this.listenTo(this.movies, 'add', this.addOne);
            this.listenTo(this.movies, 'reset', this.addAll);
            this.listenTo(this.movies, 'all', this.render);

            this.movies.add(new Series({"title": "Some title", "description": "some description"}));
            this.movies.add(new Series({"title": "Another", "description": "fhdj ksahflhdufiwh uifhueih"}));
        },
        movies: null,
        addOne: function(movie) {
            var view = new SeriesView({model: movie});
            this.$("#moviesList").append(view.render().el);
        },

        // Add all items in the **Todos** collection at once.
        addAll: function() {
            this.movies.each(this.addOne, this);
        },
        render: function() {

        }
    });

    console.log('this is executed on load');
    var application = new SmartHomeApp();
});


